========================================================================
    DYNAMIC LINK LIBRARY : tes_dll Project Overview
========================================================================

A framework to create DLL in Visual C++.
This DLL is meant to be called by using Labview.

Check on the download section where you can find a file named: lv-test.zip. A VI file is provided there and it is created with LabVIEW 2012.